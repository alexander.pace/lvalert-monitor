#!/bin/bash

# Simple script to download and setup kafka server 

# Make a source directory if it doesn't exist
mkdir -p src
mkdir -p tar
mkdir -p libs
mkdir -p config

# Get the tar:
wget http://mirrors.sonic.net/apache/kafka/2.0.0/kafka_2.11-2.0.0.tgz --directory-prefix=tar

# Extract to the source directory:
tar -xvzf tar/kafka_2.11-2.0.0.tgz -C ./src

# Copy bin stuff:
cp -rf src/kafka_2.11-2.0.0/bin/* bin/
cp -rf src/kafka_2.11-2.0.0/libs/* libs/
cp -rf src/kafka_2.11-2.0.0/config/* config/

