%define name              ligo-lvalert-monitor
%define version           0.1
%define unmangled_version 0.1
%define release           1

Summary:   LVAlert Monitoring Tools
Name:      %{name}
Version:   %{version}
Release:   %{release}%{?dist}
Source0:   %{name}-%{unmangled_version}.tar.gz
License:   GPLv2+
Group:     Development/Libraries
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix:    %{_prefix}
Vendor:    Alexander Pace <alexander.pace@ligo.org>, Leo Singer <leo.singer@ligo.org>
Url:       https://wiki.ligo.org/ligo-lvalert-monitor

BuildArch: noarch
BuildRequires: rpm-build
BuildRequires: epel-rpm-macros
BuildRequires: python-rpm-macros
BuildRequires: python3-rpm-macros
BuildRequires: python-setuptools
BuildRequires: python%{python3_pkgversion}-setuptools

%description
Monitoring Tools for the LIGO-Virgo Alert Network
This package provides monitoring tools for the LIGO-Virgo Alert
(LVAlert) network. Statistics for uptime, throughput, latency, etc.
are collected.


# -- python2-ligo-lvalert

%package -n python2-%{name}
Summary:  %{summary}
Provides: %{name}
Requires: python2-ligo-common
Requires: python2-ligo-lvalert

%{?python_provide:%python_provide python2-%{name}}

%description -n python2-%{name}
Monitoring Tools for the LIGO-Virgo Alert Network
This package provides monitoring tools for the LIGO-Virgo Alert
(LVAlert) network. Statistics for uptime, throughput, latency, etc.
are collected.


# -- python-3X-ligo-lvalert

%package -n python%{python3_pkgversion}-%{name}
Summary:  %{summary}
Requires: python%{python3_pkgversion}-ligo-common
Requires: python%{python3_pkgversion}-ligo-lvalert

%{?python_provide:%python_provide python%{python3_pkgversion}-%{name}}

%description -n python%{python3_pkgversion}-%{name}
Monitoring Tools for the LIGO-Virgo Alert Network
This package provides monitoring tools for the LIGO-Virgo Alert
(LVAlert) network. Statistics for uptime, throughput, latency, etc.
are collected.

# -- build steps

%prep
%setup -n %{name}-%{unmangled_version}

%build
# build python3 first
%py3_build
# so that the scripts come from python2
%py2_build

%install
%py2_install
%py3_install

%clean
rm -rf $RPM_BUILD_ROOT

%files -n python2-%{name}
%license COPYING
%{_bindir}/lvalert_*
%{python2_sitelib}/*
%exclude %{python_sitelib}/ligo/lvalert/*pyo

%files -n python%{python3_pkgversion}-%{name}
%license COPYING
%{python3_sitelib}/*
